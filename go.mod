module gitlab.com/smotrova-otus/libs/config

go 1.13

require (
	github.com/dsxack/go/v2 v2.2.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/viper v1.7.1
)
