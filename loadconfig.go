package configloader

import (
	dsviper "github.com/dsxack/go/v2/viper"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"net/url"
	"reflect"
	"strings"
)

func LoadConfig(filePath string, out interface{}) error {
	v := viper.New()

	if filePath != "" {
		v.SetConfigFile(filePath)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			return err
		}

		v.AddConfigPath(home)
		v.SetConfigName(".socialnetwork")
	}

	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.AutomaticEnv() // read in environment variables that match

	refOut := reflect.ValueOf(out)
	err := dsviper.BindEnvs(v, refOut.Elem().Interface())
	if err != nil {
		return err
	}

	// If a config file is found, read it in.
	if err := v.ReadInConfig(); err != nil {
		return err
	}

	return v.Unmarshal(out, viper.DecodeHook(
		mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToTimeDurationHookFunc(),
			mapstructure.StringToSliceHookFunc(","),
			StringToURLFunc(),
		),
	))
}

func StringToURLFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Kind,
		t reflect.Kind,
		data interface{}) (interface{}, error) {
		if f != reflect.String || t != reflect.Struct {
			return data, nil
		}

		raw := data.(string)
		if raw == "" {
			return url.URL{}, nil
		}

		return url.Parse(raw)
	}
}
